#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <memkind.h>
#include <libpmem.h>
#include <iostream>
#include <fstream>

#include <sys/time.h>
#include <sys/mman.h>

struct memkind *pmem_handle = MEMKIND_DEFAULT;

int main(int argc, char* argv[])
{
  int *ptr[100];
  for (int i = 0; i<100; i++) {
    ptr[i] = nullptr;
  }
  int operations=1;
  int workingset=1;
  bool exercise = false;
  bool normalRAM = true;
  bool pmdk = false;
  bool ioinstead = false;
  unsigned int SZALLOC = 1024l*1024l*1024l;
  const char *memfile = {"NOPE"};

  struct timeval t1, t2;
  double elapsedtime;

  for (int a = 1; a < argc; a++) {
    if (!strcmp(argv[a], "-size") && a<argc-1) {
      SZALLOC = atoi(argv[a+1]);
    }
    if (!strcmp(argv[a], "-operations") && a<argc-1) {
      operations = atoi(argv[a+1]);
    }
    if (!strcmp(argv[a], "-workingset") && a<argc-1) {
      workingset = atoi(argv[a+1]);
    }
    if (!strcmp(argv[a], "-exercise")) {
      exercise = true;
    }
    if (!strcmp(argv[a], "-mfile") && a<argc-1) {
      memfile = argv[a+1];
    }
    if (!strcmp(argv[a], "-pmdk")) {
      pmdk = true;
    }
    if (!strcmp(argv[a], "-ioinstead")) {
      ioinstead = true;
    }
  }

  if (strncmp(memfile, "NOPE", 4)) {
    normalRAM = false;
    if (!strncmp(memfile, "FAKE", 4)) {
      printf("USING FAKE MEMORY\n");
    } else {
      if (!pmdk) {
	printf("memkind backed by %s\n", memfile);	  
	memkind_create_pmem(memfile, 0, &pmem_handle);
      } else {
	printf("PMDK backed by %s\n", memfile);	  
      }
    }
  }

  //make something to copy around
  ptr[0] = (int*)malloc(SZALLOC);
  printf("Allocated %d bytes\n",SZALLOC);
  memset(ptr[0], 0&255, SZALLOC);
  *ptr[0] = 1;
  printf("and touched OK\n");

  gettimeofday(&t1, NULL);
  int is_pmem;
  int all_pmem = 1;
  size_t mapped_len;

  int result = 0;
  for (int o=0; o<operations; o++) {
    int dest = (o%workingset)+1;
    printf("operation %d dest %d\n", o, dest-1);
    if (ioinstead) {
      std::ofstream nextfile;
      std::string fname = memfile + std::string("deleteme_") + std::to_string(o);
      nextfile.open(fname.c_str(), std::ios::trunc|std::ios::binary);
      nextfile << "number " << o << "\n";
      nextfile.write(reinterpret_cast<const char*>(ptr[0]), SZALLOC);
      nextfile.close();
    } else {
      //clear up space for new content
      if (normalRAM) {
	free(ptr[dest]);
      } else {
	if (pmdk) {
	  if (ptr[dest]) pmem_unmap(ptr[dest], SZALLOC);
	} else {
	  memkind_free(pmem_handle, ptr[dest]);
	}
      }
      //make space for the new content
      if (normalRAM) {
	ptr[dest] = (int*)malloc(SZALLOC);
      } else {
	if (pmdk) {
	  ptr[dest] = (int*)pmem_map_file
	    (
	     memfile, SZALLOC, PMEM_FILE_CREATE|PMEM_FILE_TMPFILE,
	     0666, &mapped_len, &is_pmem
	     );
	  all_pmem &= is_pmem;
	} else {
	  ptr[dest] = (int*)memkind_malloc(pmem_handle, SZALLOC);
	}
      }
      //put something into it
      //if (!pmdk) {
        memcpy(ptr[dest], ptr[0], SZALLOC);
      //} else {
      //  pmem_memcpy(ptr[dest], ptr[0], SZALLOC, PMEM_F_MEM_WB);
	////	
	//// justmemcpy = 0.72
	//// 0 = 0.73
	//// PMEM_F_MEM_NODRAIN = 0.73
	//// PMEM_F_MEM_NOFLUSH = 1.39
	//// PMEM_F_MEM_NONTEMPORAL = 0.73
	//// PMEM_F_MEM_TEMPORAL = 0.91
	//// PMEM_F_MEM_WC = 0.73
	//// PMEM_F_MEM_WB = 0.90
      //}
      
      //optionally, do something with it
      if (exercise) {
	for (int cnt = 0; cnt < 100; cnt++) {
	  for (int byte = 16; byte < SZALLOC/sizeof(int); byte+=256) {
	    *(ptr[dest]+byte) = 1;
	  }
	}
	for (int cnt = 0; cnt < 100; cnt++) {
	  for (int byte = 16; byte < SZALLOC/sizeof(int); byte+=256) {
	    result = *(ptr[dest]+byte);
	  }
	}
      }
     
      //ensure it is written all the way up before we proceed
      if (!normalRAM) {
	if (pmdk) {
	  if (is_pmem) {
	    pmem_persist(ptr[dest], SZALLOC);
	  } else {
	    pmem_msync(ptr[dest], mapped_len);
	  }
	} else {
	  msync(ptr[dest], SZALLOC, MS_SYNC|MS_INVALIDATE); //doesn't make a difference
	}
      }

      //some assurance of correctness
      result+=*ptr[dest];
    }
  }
  printf("RESULT = %d\n", result);
  if (!normalRAM && pmdk) {
    printf("%s\n", (all_pmem ? " was persistent " : " was not persistent"));
  }
  gettimeofday(&t2, NULL);
  elapsedtime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
  elapsedtime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
  printf("t/operation %lf s\n", elapsedtime/1000.0/operations);

  free(ptr[0]);
  for (int i = 1; i<100; i++) {
    if (normalRAM) {
      free(ptr[i]);
    } else {
      if (pmdk) {
	if (ptr[i]) pmem_unmap(ptr[i], SZALLOC);
      } else {
	memkind_free(pmem_handle, ptr[i]);
      }
    }
  }
  return 0;
}
